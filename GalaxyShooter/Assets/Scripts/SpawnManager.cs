﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField]
    GameObject _enemyPrefab;
    [SerializeField]
    GameObject _conteiner;

    [SerializeField]
    GameObject[] _powerUps;

    bool isSpawing = false;
    // Start is called before the first frame update
    void Start()
    {
       
    }
    public void StartGameSpawing()
    {
        StartCoroutine(EnemySpawing());
        StartCoroutine(PowerUpSpawing());
    }

    public void isSpawingTrue()
    {
        isSpawing = true;
    }
    IEnumerator EnemySpawing()
    {
        while (isSpawing == false)
        {
            yield return new WaitForSeconds(3f);
            Vector3 randmoX = new Vector3(Random.Range(-8, 8), 8, 0);
            GameObject newEnemy = Instantiate(_enemyPrefab, randmoX, Quaternion.identity);
            newEnemy.transform.parent = _conteiner.transform;
            yield return new WaitForSeconds(Random.Range(1, 4));
        }
    }
    IEnumerator PowerUpSpawing()
    {
        while (isSpawing == false)
        {
            int ranomdPowerUpId = Random.Range(0, 3);
            Vector3 randomX = new Vector3(Random.Range(-8, 8), 8, 0);
            GameObject newPowerUp = Instantiate(_powerUps[ranomdPowerUpId], randomX, Quaternion.identity);
            newPowerUp.transform.parent = _conteiner.transform;
            yield return new WaitForSeconds(Random.Range(7, 10));
        }
    }
}
