﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    [SerializeField]
    Text _scoreText;

    [SerializeField]
    Image _liveView;
    [SerializeField]
    Sprite[] _numberSprites;
    [SerializeField]
    Text _gameOver;
    [SerializeField]
    Text _restart;

    GameManager _gameManager;
    // Start is called before the first frame update
    void Start()
    {
        _scoreText = transform.GetChild(0).GetComponent<Text>();
        _gameOver = transform.GetChild(1).GetComponent<Text>();
        _restart = transform.GetChild(2).GetComponent<Text>();

        _gameManager = GameObject.FindGameObjectWithTag("Canvas").GetComponent<GameManager>();
        if (_gameManager == null)
        {
            Debug.Log("GameManager is null in UiManager");
        }
    }

   
    public void ScoreView (int scoreValue)
    {
        _scoreText.text = "Score: " + scoreValue.ToString();
    }
    public void LiveView(int currentLive)
    {
        _liveView.sprite = _numberSprites[currentLive];

        if (currentLive < 1)
        {
            TextALLView();
        }
    }

    public void TextALLView()
    {
        _gameManager.RestartGameTrue();
        _gameOver.gameObject.SetActive(true);
        _restart.gameObject.SetActive(true);
        StartCoroutine(GameOverFlick());
    }

    IEnumerator GameOverFlick()
    {
        while (true)
        {
            yield return new WaitForSeconds(1.2f);
            _gameOver.gameObject.SetActive(false);
            yield return new WaitForSeconds(1.2f);
            _gameOver.gameObject.SetActive(true);
        }
    }
}
