﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    GameObject _tipleShotPrefab;
    [SerializeField]
    GameObject _laserPrefab;
    [SerializeField]
    GameObject _ShiedlVis;
    [SerializeField]
    bool isTriple;
    [SerializeField]
    bool isSpeed;
    [SerializeField]
    bool isShield;
    [SerializeField]
    float _speedPlayer = 5f;
    [SerializeField]
    float _canFire = -1;
    [SerializeField]
    float _nextTimeShot = 0.2f;

    [SerializeField]
    int _playerLive = 3;
    [SerializeField]
    int _score;
    [SerializeField]
    AudioClip _laserAudio;

    UiManager _uiManager;
    AudioSource _audioSource;
    
    // Start is called before the first frame update
    void Start()
    {
        _uiManager = GameObject.FindGameObjectWithTag("Canvas").GetComponent<UiManager>();
        if (_uiManager == null)
        {
            Debug.Log("UiManager is null in Player");
        }

        _audioSource = GetComponent<AudioSource>();
        if (_audioSource == null)
        {
            Debug.Log("_audioSource is null in Player");
        }
        else
        {
            _audioSource.clip = _laserAudio;
        }
    }

    // Update is called once per frame
    void Update()
    {
        PlayerMoving();
        PlayerZone();
        PlayerShothing();
    }
    public void PlayerMoving()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal, vertical, 0);
        Vector3 velocity = direction * _speedPlayer;
        transform.Translate(velocity * Time.deltaTime);
    }

    public void PlayerZone()
    {
        if (transform.position.y > 0)
        {
            transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        }
        else if (transform.position.y < -3.5f)
        {
            transform.position = new Vector3(transform.position.x, -3.5f, transform.position.z);
        }

        if (transform.position.x > 11)
        {
            transform.position = new Vector3(-11, transform.position.y, transform.position.z);
        }
        else if (transform.position.x < -11)
        {
            transform.position = new Vector3(11, transform.position.y, transform.position.z);
        }
    }
    public void PlayerShothing()
    {
        if (Input.GetKeyDown(KeyCode.Space) && Time.time >_canFire)
        {
            _canFire = Time.time + _nextTimeShot;

            if (isTriple == true)
            {
                Instantiate(_tipleShotPrefab, transform.position, Quaternion.identity);
            }
            else
            {
                Instantiate(_laserPrefab, transform.position + new Vector3(0, 1, 0), Quaternion.identity);
            }
            _audioSource.Play();
        }
    }

    public void PlayerDamage(int valueDamage)
    {
        if (isShield == true)
        {
            _ShiedlVis.gameObject.SetActive(false);
            isShield = false;
            return;
        }

        _playerLive -= valueDamage;
        _uiManager.LiveView(_playerLive);

        if (_playerLive < 1)
        {
            Destroy(this.gameObject);
        }
    }

    public void AddPoints(int pointValue)
    {
        _score += pointValue;
        _uiManager.ScoreView(_score);
    }






    public void TripleTrue()
    {
        isTriple = true;
        StartCoroutine(TripleActive());
    }
    public void SpeedTrue()
    {
        isSpeed = true;
        _speedPlayer *= 2.5f;
        StartCoroutine(SpeedActive());
    }
    public void ShieldTrue()
    {
        isShield = true;
        _ShiedlVis.gameObject.SetActive(true);
    }

    IEnumerator TripleActive()
    {
        yield return new WaitForSeconds(3f);
        isTriple = false;
    }
    IEnumerator SpeedActive()
    {
        yield return new WaitForSeconds(3f);
        _speedPlayer /= 2.5f;
        isSpeed = false;
    }
}
