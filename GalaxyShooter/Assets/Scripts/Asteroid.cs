﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    [SerializeField]
    float _speed=5f;
    [SerializeField]
    GameObject _expoVis;

    SpawnManager _spawnManager;
    AudioSource _audioSource;
    // Start is called before the first frame update
    void Start()
    {
        _spawnManager = GameObject.FindGameObjectWithTag("SpawnManager").GetComponent<SpawnManager>();

        if (_spawnManager == null)
        {
            Debug.Log("SpanwManager is null");
        }

        _audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.forward * _speed * Time.deltaTime);   
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Laser"))
        {
            Destroy(this.gameObject,1.3f);
            Destroy(collision.gameObject);
            Instantiate(_expoVis, transform.position, Quaternion.identity);
            _spawnManager.StartGameSpawing();
            _audioSource.Play();
        }
    }
}
