﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    
    float _speedLaser = 7f;

    [SerializeField]
    bool isTriple;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.up * _speedLaser * Time.deltaTime);

        if (transform.position.y > 8f)
        {
            if (gameObject.transform.parent != null)
            {
                Destroy(transform.parent.gameObject);
            }

            Destroy(this.gameObject);
        }
    }
   
    
}
