﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    float _speedEnemy = 5f;

    Player _player;
    UiManager _uiManager;
    Animator _anim;

    AudioSource _audioSource;
    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        if (_player == null)
        {
            Debug.Log("Player is null in Enemy");
        }
        _uiManager = GameObject.FindGameObjectWithTag("Canvas").GetComponent<UiManager>();
        if (_uiManager == null)
        {
            Debug.Log("UiManager is null in Enemy");
        }


        _anim = GetComponent<Animator>();
        _audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.down * _speedEnemy * Time.deltaTime);

        if (transform.position.y < -8.5f)
        {
            Vector3 randomX = new Vector3(Random.Range(-8, 8), 8, 0);
            transform.position = randomX;
        }
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            _player.PlayerDamage(1);
            _speedEnemy = 0;
            _anim.SetTrigger("IsDeadsEnemy");
            _audioSource.Play();
            Destroy(this.gameObject,2.5f);
          
        }
        if (collision.gameObject.CompareTag("Laser"))
        {
           
            _player.AddPoints(1);
            _anim.SetTrigger("IsDeadsEnemy");
            _audioSource.Play();
            _speedEnemy = 0f;
            Destroy(collision.gameObject);
            Destroy(this.gameObject,2.5f);
        }
    }
}
