﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{

    [SerializeField]
    float _speedPowerUp = 3f;
    [SerializeField]
    int powerUpID;

    Player player;


    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        if (player == null)
        {
            Debug.Log("Player is null in PowerUp");
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.down * _speedPowerUp * Time.deltaTime);
        if (transform.position.y < -8f)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            switch (powerUpID)
            {
                case 0:
                    player.TripleTrue(); 
                    break;
                case 1:
                    player.SpeedTrue();
                    break;
                case 2:
                    player.ShieldTrue();
                    break;
                default:
                    Debug.Log("Default");
                    break;
            }
            Destroy(this.gameObject);
        } 
    }
}
